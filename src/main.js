import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

import VueTextAreaAutoSize from 'vue-textarea-autosize'
import firebase from 'firebase/app';
import 'firebase/firestore';

Vue.use(VueTextAreaAutoSize);

const firebaseConfig = {
    apiKey: "AIzaSyDItU_ClwTRNoeLKsiV-3n_jANGrIcWAhU",
    authDomain: "calendario-firebase.firebaseapp.com",
    databaseURL: "https://calendario-firebase.firebaseio.com",
    projectId: "calendario-firebase",
    storageBucket: "calendario-firebase.appspot.com",
    messagingSenderId: "669445259058",
    appId: "1:669445259058:web:c14ebdb438f4e55558e12b"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export const db = firebase.firestore();

Vue.config.productionTip = false

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app')